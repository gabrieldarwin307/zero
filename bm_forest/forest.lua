
-- safe defaults when missing optional depends 
local dungeon_stair = "zr_stone:stone";
local dungeon_alt = "zr_stone:mossycobble";

if(minetest.get_modpath("zr_stair")) ~= nil then
	dungeon_stair = "zr_stone:stair_cobble";
end

local cave_liquid = {"zr_water:source"};
if(minetest.get_modpath("zr_lava")) ~= nil then
	cave_liquid = {"zr_water:source", "zr_lava:source"};
end


minetest.register_biome({
	name = "bm_forest",
	node_top = "zr_dirt:grass",
	depth_top = 1,
	node_filler = "zr_dirt:dirt",
	depth_filler = 3,
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = dungeon_alt,
	node_dungeon_stair = dungeon_stair,
	y_max = 31000,
	y_min = 1,
	heat_point = 60,
	humidity_point = 68,
})

minetest.register_biome({
	name = "bm_forest_shore",
	node_top = "zr_dirt:dirt",
	depth_top = 1,
	node_filler = "zr_dirt:dirt",
	depth_filler = 3,
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = dungeon_alt,
	node_dungeon_stair = dungeon_stair,
	y_max = 0,
	y_min = -1,
	heat_point = 60,
	humidity_point = 68,
})

minetest.register_biome({
	name = "bm_forest_ocean",
	node_top = "zr_sand:sand",
	depth_top = 1,
	node_filler = "zr_sand:sand",
	depth_filler = 3,
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2,
	node_cave_liquid = "zr_water:source",
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = dungeon_alt,
	node_dungeon_stair = dungeon_stair,
	vertical_blend = 1,
	y_max = -2,
	y_min = -255,
	heat_point = 60,
	humidity_point = 68,
})

minetest.register_biome({
	name = "bm_forest_under",
	node_cave_liquid = cave_liquid,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = dungeon_alt,
	node_dungeon_stair = dungeon_stair,
	y_max = -256,
	y_min = -31000,
	heat_point = 60,
	humidity_point = 68,
})

if (minetest.get_modpath("zr_apple") ~= nil) then
	zr_apple.add_to_biome("bm_forest")
	zr_apple.add_bush_to_biome("bm_forest")
	zr_apple.add_log_to_biome("bm_forest")
end

if (minetest.get_modpath("zr_aspen") ~= nil) then
	zr_aspen.add_to_biome("bm_forest")
	zr_aspen.add_log_to_biome("bm_forest")
end

if (minetest.get_modpath("zr_mushroom") ~= nil) then
	zr_mushroom.add_all_to_biome("bm_forest")
end

if (minetest.get_modpath("zr_papyrus") ~= nil) then
	zr_papyrus.add_to_biome_on_dirt("bm_forest_shore")
end

if(minetest.get_modpath("zr_bug")) ~= nil then
	zr_bug.add_firefly_to_biome("bm_forest")
end

if(minetest.get_modpath("zr_kelp")) ~= nil then
	zr_kelp.add_to_biome("bm_forest_ocean")
end


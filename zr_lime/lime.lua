
local S = minetest.get_translator("zr_lime")

zr_lime.sounds = {
    footstep = {name = "zr_lime_footstep", gain = 0.3},
    dig = {name = "zr_lime_dig", gain = 0.5},
    dug = {name = "zr_lime_footstep", gain = 1.0},
    place = {name = "zr_lime_place", gain = 1.0},
}

-- LIMESTONE
minetest.register_node("zr_lime:stone", {
    description = S("Stone"),
    tiles = {"zr_lime_stone.png"},
    groups = {cracky = 3, stone = 1},
    drop = "zr_lime:cobble",
    legacy_mineral = true,
    sounds = zr_lime.sounds,
})

-- COBBLE 
minetest.register_node("zr_lime:cobble", {
    description = S("Limestone Cobblestone"),
    tiles = {"zr_lime_cobble.png"},
    is_ground_content = false,
    groups = {cracky = 3, stone = 2, blast_loss = 3},
    sounds = zr_lime.sounds,
})

minetest.register_craft({
	type = "cooking",
	output = "zr_lime:stone",
	recipe = "zr_lime:cobble",
})

-- MOSSY COBBLE
minetest.register_node("zr_lime:mossycobble", {
	description = S("Mossy Limestone Cobblestone"),
	tiles = {"zr_lime_mossycobble.png"},
	is_ground_content = false,
	groups = {cracky = 3, stone = 1},
	sounds = zr_lime.sounds,
})

minetest.register_craft({
	type = "cooking",
	output = "zr_lime:stone",
	recipe = "zr_lime:mossycobble",
})


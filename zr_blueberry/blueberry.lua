
local S = minetest.get_translator("zr_blueberry");

minetest.register_node("zr_blueberry:leaves_with_berries", {
	description = S("Blueberry Bush Leaves with Berries"),
	drawtype = "allfaces_optional",
	tiles = {"zr_blueberry_bush_leaves.png^zr_blueberry_overlay.png"},
	paramtype = "light",
    groups = {snappy = 3, leafdecay = 3, flammable = 2, leaves = 1, oddly_breakable_by_hand=3, dig_immediate=3},
	drop = "zr_blueberry:blueberry",
	sounds = zr_wood.leaves_sounds,
	node_dig_prediction = "zr_blueberry:leaves",

	after_dig_node = function(pos, oldnode, oldmetadata, digger)
		minetest.set_node(pos, {name = "zr_blueberry:leaves"})
		minetest.get_node_timer(pos):start(math.random(300, 1500))
	end,
})
minetest.register_alias("blueberry:leaves_with_berries", "zr_blueberry:leaves_with_berries")

minetest.register_node("zr_blueberry:leaves", {
	description = S("Blueberry Bush Leaves"),
	drawtype = "allfaces_optional",
	tiles = {"zr_blueberry_bush_leaves.png"},
	paramtype = "light",
    groups = {snappy = 3, leafdecay = 3, flammable = 2, leaves = 1, oddly_breakable_by_hand=3},
	drop = {
		max_items = 1,
		items = {
			{items = {"zr_blueberry:sapling"}, rarity = 5},
			{items = {"zr_blueberry:leaves"}}
		}
	},
	sounds = zr_wood.leaves_sounds,

	on_timer = function(pos, elapsed)
		if minetest.get_node_light(pos) < 11 then
			minetest.get_node_timer(pos):start(200)
		else
			minetest.set_node(pos, {name = "zr_blueberry:leaves_with_berries"})
		end
	end,

    after_place_node = zr_wood.after_place_leaves,
})
minetest.register_alias("blueberry:leaves", "zr_blueberry:leaves")

minetest.register_craftitem("zr_blueberry:blueberry", {
	description = S("Blueberries"),
	inventory_image = "zr_blueberry.png",
	groups = {food_blueberries = 1, food_berry = 1},
	on_use = minetest.item_eat(2),
})
minetest.register_alias("blueberry:blueberry", "zr_blueberry:blueberry")
minetest.register_alias("blueberry", "zr_blueberry:blueberry")


local river_bed = {"zr_snow:ice"};
if(minetest.get_modpath("zr_gravel")) ~= nil then
	river_bed = {"zr_gravel:gravel"};
end

minetest.register_biome({
	name = "bm_icesheet",
	node_dust = "zr_snow:block",
	node_top = "zr_snow:block",
	depth_top = 1,
	node_filler = "zr_snow:block",
	depth_filler = 3,
	node_stone = "zr_snow:cave_ice",
	node_water_top = "zr_snow:ice",
	depth_water_top = 10,
	node_river_water = "zr_snow:ice",
	node_riverbed = "zr_gravel:gravel",
	depth_riverbed = 2,
	node_dungeon = "zr_snow:ice",
	node_dungeon_stair = "zr_snow:stair_ice",
	y_max = 31000,
	y_min = -8,
	heat_point = 0,
	humidity_point = 73,
})

if(minetest.get_modpath("icetower")) ~= nil then
	icetower.add_to_biome("bm_icesheet")
end

minetest.register_biome({
	name = "bm_icesheet_ocean",
	node_dust = "zr_snow:block",
	node_top = "zr_sand:sand",
	depth_top = 1,
	node_filler = "zr_sand:sand",
	depth_filler = 3,
	node_water_top = "zr_snow:ice",
	depth_water_top = 10,
	node_cave_liquid = "zr_water:source",
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	y_max = -9,
	y_min = -255,
	heat_point = 0,
	humidity_point = 73,
})

local cave_liquid = {"zr_water:source"};
if(minetest.get_modpath("zr_lava")) ~= nil then
	cave_liquid = {"zr_water:source", "zr_lava:source"};
end

minetest.register_biome({
	name = "bm_icesheet_under",
	node_cave_liquid = cave_liquid,
	node_dungeon = "zr_stone:cobble",
	node_dungeon_alt = "zr_stone:mossycobble",
	node_dungeon_stair = "zr_stone:stair_cobble",
	y_max = -256,
	y_min = -31000,
	heat_point = 0,
	humidity_point = 73,
})


local S = minetest.get_translator("zr_grass")

minetest.register_node("zr_grass:marram", {
	description = S("Marram Grass"),
	drawtype = "plantlike",
	waving = 1,
	tiles = {"zr_grass_marram_1.png"},
	inventory_image = "zr_grass_marram_1.png",
	wield_image = "zr_grass_marram_1.png",
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	groups = {snappy = 3, flammable = 3, flora = 1, grass = 1, marram = 1,
		attached_node = 1},
	sounds = zr_dirt.grass_sounds,
	selection_box = {
		type = "fixed",
		fixed = {-6 / 16, -0.5, -6 / 16, 6 / 16, -0.25, 6 / 16},
	},

	on_place = function(itemstack, placer, pointed_thing)
		-- place a random marram grass node
		local stack = ItemStack("zr_grass:marram_" .. math.random(1, 3))
		local ret = minetest.item_place(stack, placer, pointed_thing)
		return ItemStack("zr_grass:marram " ..
			itemstack:get_count() - (1 - ret:get_count()))
	end,
})
minetest.register_alias("grass:marram", "zr_grass:marram")

for i = 2, 3 do
	minetest.register_node("zr_grass:marram_" .. i, {
		description = S("Marram Grass"),
		drawtype = "plantlike",
		waving = 1,
		tiles = {"zr_grass_marram_" .. i .. ".png"},
		inventory_image = "zr_grass_marram_" .. i .. ".png",
		wield_image = "zr_grass_marram_" .. i .. ".png",
		paramtype = "light",
		sunlight_propagates = true,
		walkable = false,
		buildable_to = true,
		groups = {snappy = 3, flammable = 3, flora = 1, attached_node = 1,
			grass = 1, marram = 1, not_in_creative_inventory = 1},
		drop = "zr_grass:marram",
		sounds = zr_dirt.grass_sounds,
		selection_box = {
			type = "fixed",
			fixed = {-6 / 16, -0.5, -6 / 16, 6 / 16, -0.25, 6 / 16},
		},
	})
end


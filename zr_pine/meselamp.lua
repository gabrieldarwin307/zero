
local S = minetest.get_translator("zr_pine");

zr_mese.register_lampost("zr_pine:mese_post_light", {
	description = S("Pine Wood Mese Post Light"),
	texture = "zr_pine_fence.png",
	material = "zr_pine:wood",
})

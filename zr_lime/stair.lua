
-- stone stairs (only ran if mod stair is included)
local S = minetest.get_translator("zr_stairs")

-- Register default stairs and slabs

zr_stair.register_all("zr_lime:stone_",{ recipeitem = "zr_lime:stone" }) 
zr_stair.register_all("zr_lime:cobble_",{ recipeitem = "zr_lime:cobble" }) 
zr_stair.register_all("zr_lime:mossycobble_",{ recipeitem = "zr_lime:mossycobble" }) 

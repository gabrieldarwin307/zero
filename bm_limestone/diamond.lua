local S = minetest.get_translator("zr_diamond");

minetest.register_node("bm_limestone:diamond_ore", {
	description = S("Diamond Ore"),
	tiles = {"zr_lime_stone.png^zr_diamond_mineral.png"},
	groups = {cracky = 1},
	drop = "zr_diamond:diamond",
	sounds = zr_stone.sounds,
})

-- ore distribution
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "bm_limestone:diamond_ore",
	wherein        = "zr_lime:stone",
	clust_scarcity = 15 * 15 * 15,
	clust_num_ores = 4,
	clust_size     = 3,
	y_max          = 31000,
	y_min          = 1025,
})
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "bm_limestone:diamond_ore",
	wherein        = "zr_lime:stone",
	clust_scarcity = 17 * 17 * 17,
	clust_num_ores = 4,
	clust_size     = 3,
	y_max          = -1024,
	y_min          = -2047,
})
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "bm_limestone:diamond_ore",
	wherein        = "zr_lime:stone",
	clust_scarcity = 15 * 15 * 15,
	clust_num_ores = 4,
	clust_size     = 3,
	y_max          = -2048,
	y_min          = -31000,
})

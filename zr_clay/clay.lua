
local S = minetest.get_translator("zr_clay");

minetest.register_node("zr_clay:clay", {
	description = S("Clay"),
	tiles = {"zr_clay.png"},
	groups = {crumbly = 3},
	drop = "zr_clay:lump 4",
	sounds = zr_dirt.sounds,
})
minetest.register_alias("clay:clay", "zr_clay:clay")
minetest.register_alias("clay", "zr_clay:clay")


minetest.register_craftitem("zr_clay:lump", {
	description = S("Clay Lump"),
	inventory_image = "zr_clay_lump.png",
})
minetest.register_alias("clay:lump", "zr_clay:lump")

minetest.register_craft({
	output = "zr_clay:clay",
	recipe = {
		{"zr_clay:lump", "zr_clay:lump"},
		{"zr_clay:lump", "zr_clay:lump"},
	}
})

minetest.register_craft({
	output = "zr_clay:lump 4",
	recipe = {
		{"zr_clay:clay"},
	}
})

minetest.register_ore({
	ore_type        = "blob",
	ore             = "zr_clay:clay",
	wherein         = {"zr_sand:sand"},
	clust_scarcity  = 16 * 16 * 16,
	clust_size      = 5,
	y_max           = 0,
	y_min           = -15,
	noise_threshold = 0.0,
	noise_params    = {
		offset = 0.5,
		scale = 0.2,
		spread = {x = 5, y = 5, z = 5},
		seed = -316,
		octaves = 1,
		persist = 0.0
	},
})

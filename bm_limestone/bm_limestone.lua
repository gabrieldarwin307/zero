minetest.register_biome({
	name = "bm_limestone",
	node_top = "zr_dirt:grass",
	depth_top = 1, 
	node_stone = "zr_lime:stone",
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2, 
	node_dungeon = "zr_lime:cobble",
	node_dungeon_alt = "zr_lime:mossycobble",
	node_dungeon_stair = "zr_lime:stair_stone",
	y_max = 31000,
	y_min = 6, 
	heat_point = 60,
	humidity_point = 37,
})   

minetest.register_biome({
	name = "bm_limestone_dunes",
	node_top = "zr_sand:sand",
	depth_top = 1, 
	node_filler = "zr_sand:sand",
	depth_filler = 2, 
	node_stone = "zr_lime:stone",
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2, 
	node_dungeon = "zr_lime:cobble",
	node_dungeon_alt = "zr_lime:mossycobble",
	node_dungeon_stair = "zr_lime:stair_cobble",
	vertical_blend = 1, 
	y_max = 5, 
	y_min = 4, 
	heat_point = 50,
	humidity_point = 35,
})   

minetest.register_biome({
	name = "bm_limestone_ocean",
	node_top = "zr_sand:sand",
	depth_top = 1, 
	node_filler = "zr_sand:sand",
	depth_filler = 3, 
	node_stone = "zr_lime:stone",
	node_riverbed = "zr_sand:sand",
	depth_riverbed = 2, 
	node_cave_liquid = "zr_water:source",
	node_dungeon = "zr_lime:cobble",
	node_dungeon_alt = "zr_lime:mossycobble",
	node_dungeon_stair = "zr_lime:stair_cobble",
	y_max = 3, 
	y_min = -255,
	heat_point = 50,
	humidity_point = 35,
})   

minetest.register_biome({
	name = "bm_limestone_under",
	node_cave_liquid = "zr_water:river_source",
	node_dungeon = "zr_lime:cobble",
	node_dungeon_alt = "zr_lime:mossycobble",
	node_dungeon_stair = "zr_lime:stair_cobble",
	node_stone = "zr_lime:stone",
	y_max = -256,
	y_min = -31000,
	heat_point = 50,
	humidity_point = 35,
})   

zr_lime.add_stala_to_biome("bm_limestone_under")


if(minetest.get_modpath("zr_grass")) ~= nil then
	zr_grass.add_to_biome("bm_limestone")
end

if(minetest.get_modpath("zr_flower")) ~= nil then
	zr_flower.add_all_flowers_to_biome("bm_limestone")
end

if(minetest.get_modpath("zr_bug")) ~= nil then
	zr_bug.add_butterfly_to_biome("bm_limestone")
	zr_bug.add_firefly_to_biome("bm_limestone")
end

if(minetest.get_modpath("zr_papyrus")) ~= nil then
	zr_papyrus.add_to_biome_on_dirt("bm_limestone_ocean")
end

if(minetest.get_modpath("zr_kelp")) ~= nil then
	zr_kelp.add_to_biome("bm_limestone_ocean")
end

if(minetest.get_modpath("zr_blueberry")) ~= nil then
	zr_blueberry.add_to_biome("bm_limestone")
end


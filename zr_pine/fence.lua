
local S = minetest.get_translator("zr_pine");

-- FENCE
zr_fence.register_fence("zr_pine:fence", {                                                                       
    description = S("Pine Wood Fence"),
    texture = "zr_pine_fence.png",
    material = "zr_pine:wood",
    groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
    sounds = zr_wood.sounds,
})
minetest.register_alias("pine:fence", "zr_pine:fence")

zr_fence.register_fence_rail("zr_pine:fence_rail", {                                                                       
    description = S("Pine Wood Fence Rail"),
    texture = "zr_pine_fence_rail.png",
    material = "zr_pine:wood",
    groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
    sounds = zr_wood.sounds,
})
minetest.register_alias("pine:fence_rail", "zr_pine:fence_rail")

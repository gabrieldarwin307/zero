

local S = minetest.get_translator("zr_iron");

zr_sign.register("zr_iron:sign", {
	description = S("Steel Sign"), 
	tiles = {"zr_iron_sign_wall.png"},
	inventory_image = "zr_iron_sign.png",
	wield_image = "zr_iron_sign.png",
	sounds = zr_metal.sounds,
	groups = {cracky = 2, attached_node = 1},
	recipeitem = "zr_iron:ingot",
})
minetest.register_alias("iron:sign", "zr_iron:sign")

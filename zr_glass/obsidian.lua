
local S = minetest.get_translator("zr_glass")
minetest.register_node("zr_glass:obsidian", {
	description = S("Obsidian"),
	tiles = {"zr_glass_obsidian.png"},
	sounds = zr_glass.sounds,
	groups = {cracky = 1, level = 2},
})
minetest.register_alias("glass:obsidian", "zr_glass:obsidian")
minetest.register_alias("obsidian", "zr_glass:obsidian")

minetest.register_node("zr_glass:obsidian_brick", {
	description = S("Obsidian Brick"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"zr_glass_obsidian_brick.png"},
	is_ground_content = false,
	sounds = zr_glass.sounds,
	groups = {cracky = 1, level = 2},
})
minetest.register_alias("glass:obsidian_brick", "zr_glass:obsidian_brick")
minetest.register_alias("obsidian_brick", "zr_glass:obsidian_brick")

minetest.register_node("zr_glass:obsidian_block", {
	description = S("Obsidian Block"),
	tiles = {"zr_glass_obsidian_block.png"},
	is_ground_content = false,
	sounds = zr_glass.sounds,
	groups = {cracky = 1, level = 2},
})
minetest.register_alias("glass:obsidian_block", "zr_glass:obsidian_block")
minetest.register_alias("obsidian_block", "zr_glass:obsidian_block")

minetest.register_node("zr_glass:obsidian_glass", {
	description = S("Obsidian Glass"),
	drawtype = "glasslike_framed_optional",
	tiles = {"zr_glass_obsidian_glass.png", "zr_glass_obsidian_glass_detail.png"},
	use_texture_alpha = "clip",
	paramtype = "light",
	paramtype2 = "glasslikeliquidlevel",
	is_ground_content = false,
	sunlight_propagates = true,
	sounds = zr_glass.sounds,
	groups = {cracky = 3},
})
minetest.register_alias("glass:obsidian_glass", "zr_glass:obsidian_glass")
minetest.register_alias("obsidian_glass", "zr_glass:obsidian_glass")

minetest.register_craftitem("zr_glass:obsidian_shard", {
	description = S("Obsidian Shard"),
	inventory_image = "zr_glass_obsidian_shard.png",
})
minetest.register_alias("glass:obsidian_shard", "zr_glass:obsidian_shard")
minetest.register_alias("obsidian_shard", "zr_glass:obsidian_shard")

minetest.register_craft({
	output = "zr_glass:obsidian_shard 9",
	recipe = {
		{"zr_glass:obsidian"}
	}
})

minetest.register_craft({
	output = "zr_glass:obsidian",
	recipe = {
		{"zr_glass:obsidian_shard", "zr_glass:obsidian_shard", "zr_glass:obsidian_shard"},
		{"zr_glass:obsidian_shard", "zr_glass:obsidian_shard", "zr_glass:obsidian_shard"},
		{"zr_glass:obsidian_shard", "zr_glass:obsidian_shard", "zr_glass:obsidian_shard"},
	}
})

minetest.register_craft({
	output = "zr_glass:obsidian_brick 4",
	recipe = {
		{"zr_glass:obsidian", "zr_glass:obsidian"},
		{"zr_glass:obsidian", "zr_glass:obsidian"}
	}
})

minetest.register_craft({
	output = "zr_glass:obsidian_block 9",
	recipe = {
		{"zr_glass:obsidian", "zr_glass:obsidian", "zr_glass:obsidian"},
		{"zr_glass:obsidian", "zr_glass:obsidian", "zr_glass:obsidian"},
		{"zr_glass:obsidian", "zr_glass:obsidian", "zr_glass:obsidian"},
	}
})

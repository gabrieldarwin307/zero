-- Copper

local S = minetest.get_translator("zr_copper");

minetest.register_node("bm_limestone:copper_ore", {
	description = S("Copper Ore"),
	tiles = {"zr_lime_stone.png^zr_copper_mineral.png"},
	groups = {cracky = 2},
	drop = "zr_copper:lump",
	sounds = zr_stone.sounds,
})

minetest.register_ore({
	ore_type       = "scatter",
	ore            = "bm_limestone:copper_ore",
	wherein        = "zr_lime:stone",
	clust_scarcity = 9 * 9 * 9,
	clust_num_ores = 5,
	clust_size     = 3,
	y_max          = 31000,
	y_min          = 1025,
})
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "bm_limestone:copper_ore",
	wherein        = "zr_lime:stone",
	clust_scarcity = 12 * 12 * 12,
	clust_num_ores = 4,
	clust_size     = 3,
	y_max          = -64,
	y_min          = -127,
})
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "bm_limestone:copper_ore",
	wherein        = "zr_lime:stone",
	clust_scarcity = 9 * 9 * 9,
	clust_num_ores = 5,
	clust_size     = 3,
	y_max          = -128,
	y_min          = -31000,
})

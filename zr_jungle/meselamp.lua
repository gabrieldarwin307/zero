
local S = minetest.get_translator("zr_jungle");

zr_mese.register_lampost("zr_jungle:mese_post_light", {
	description = S("Jungle Wood Mese Post Light"),
	texture = "zr_jungle_fence.png",
	material = "zr_jungle:wood",
})
minetest.register_alias("jungle:mese_post_light", "zr_jungle:mese_post_light")

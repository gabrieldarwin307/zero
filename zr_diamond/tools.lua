local S = minetest.get_translator("zr_diamond");

zr_tools.register_pickaxe("zr_diamond:pick", {
	description = S("Diamond Pickaxe"),
	inventory_image = "zr_diamond_pick.png",
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level=3,
		groupcaps={
			cracky = {times={[1]=2.0, [2]=1.0, [3]=0.50}, uses=30, maxlevel=3},
		},
		damage_groups = {fleshy=5},
	},
	recipeitem = "zr_diamond:diamond",
})
minetest.register_alias("diamond:pick", "zr_diamond:pick")

zr_tools.register_shovel("zr_diamond:shovel", {
	description = S("Diamond Shovel"),
	inventory_image = "zr_diamond_shovel.png",
	wield_image = "zr_diamond_shovel.png^[transformR90",
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=1,
		groupcaps={
			crumbly = {times={[1]=1.10, [2]=0.50, [3]=0.30}, uses=30, maxlevel=3},
		},
		damage_groups = {fleshy=4},
	},
	recipeitem = "zr_diamond:diamond",
})
minetest.register_alias("diamond:shovel", "zr_diamond:shovel")

zr_tools.register_axe("zr_diamond:axe", {
	description = S("Diamond Axe"),
	inventory_image = "zr_diamond_axe.png",
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level=1,
		groupcaps={
			choppy={times={[1]=2.10, [2]=0.90, [3]=0.50}, uses=30, maxlevel=3},
		},
		damage_groups = {fleshy=7},
	},
	recipeitem = "zr_diamond:diamond",
})
minetest.register_alias("diamond:axe", "zr_diamond:axe")

zr_tools.register_sword("zr_diamond:sword", {
	description = S("Diamond Sword"),
	inventory_image = "zr_diamond_sword.png",
	tool_capabilities = {
		full_punch_interval = 0.7,
		max_drop_level=1,
		groupcaps={
			snappy={times={[1]=1.90, [2]=0.90, [3]=0.30}, uses=40, maxlevel=3},
		},
		damage_groups = {fleshy=8},
	},
	recipeitem = "zr_diamond:diamond",
})
minetest.register_alias("diamond:sword", "zr_diamond:sword")

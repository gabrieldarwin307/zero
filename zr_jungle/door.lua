local S = minetest.get_translator("zr_jungle");

-- GATE
zr_door.register_gate("zr_jungle:gate", {
	description = S("Jungle Wood Fence Gate"),
	texture = "zr_jungle_wood.png",
	material = "zr_jungle:wood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = zr_wood.sounds,
})
minetest.register_alias("jungle:gate", "zr_jungle:gate")

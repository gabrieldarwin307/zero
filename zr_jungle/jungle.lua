
local S = minetest.get_translator("zr_jungle")

minetest.register_node("zr_jungle:tree", {
	description = S("Jungle Tree"),
	tiles = {"zr_jungle_tree_top.png", "zr_jungle_tree_top.png",
		"zr_jungle_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree = 1, choppy = 2, oddly_breakable_by_hand = 1, flammable = 2},
	sounds = zr_wood.sounds,

	on_place = minetest.rotate_node
})
minetest.register_alias("jungle:tree", "zr_jungle:tree")

minetest.register_node("zr_jungle:wood", {
	description = S("Jungle Wood Planks"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"zr_jungle_wood.png"},
	is_ground_content = false,
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, wood = 1},
	sounds = zr_wood.sounds,
})
minetest.register_alias("jungle:wood", "zr_jungle:wood")

minetest.register_node("zr_jungle:leaves", {
	description = S("Jungle Tree Leaves"),
	drawtype = "allfaces_optional",
	tiles = {"zr_jungle_leaves.png"},
	special_tiles = {"zr_jungle_leaves_simple.png"},
	waving = 1,
	paramtype = "light",
	is_ground_content = false,
	groups = {snappy = 3, leafdecay = 3, flammable = 2, leaves = 1},
	drop = {
		max_items = 1,
		items = {
			{items = {"zr_jungle:sapling"}, rarity = 20},
			{items = {"zr_jungle:leaves"}}
		}
	},
	sounds = zr_wood.leaves_sounds,

	after_place_node = zr_wood.after_place_leaves,
})
minetest.register_alias("jungle:leaves", "zr_jungle:leaves")

zr_wood.register_leafdecay({
	trunks = {"zr_jungle:tree"},
	leaves = {"zr_jungle:leaves"},
	radius = 2,
})

minetest.register_craft({
    output = "zr_jungle:wood 4",
    recipe = {
        {"zr_jungle:tree"},
    }
})

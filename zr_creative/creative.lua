
local S = minetest.get_translator("zr_creative")

-- Make creative 'grantable' for mixed survival/creative worlds
minetest.register_privilege("creative", {
	description = S("Allow player to use creative inventory"),
	give_to_singleplayer = false,
	give_to_admin = false,
	on_grant = function(name)
		local player = minetest.get_player_by_name(name)	
		zr_creative.set_inv(player)
	end,
	on_revoke = function(name)
		local player = minetest.get_player_by_name(name)	
		player:get_inventory():set_size("main",8*4)
		if zr_inv then 
			zr_inv.set_inv(player)
		else
			player:set_inventory_formspec("")
		end
	end
})

-- override minetest api for checking if creative 
local old_is_creative_enabled = minetest.is_creative_enabled
function minetest.is_creative_enabled(name)
	if name == "" then
		return old_is_creative_enabled(name)
	end
	return minetest.check_player_privs(name, {creative = true}) or
		old_is_creative_enabled(name)
end

-- don't show zr_inv inventory if creative is enabled
if(minetest.get_modpath("zr_inv")) ~= nil then
	-- default to existing zr_inv if creative is not set
	local old_set_inv = zr_inv.set_inv
	zr_inv.set_inv = function(player)
		local p_name = player:get_player_name()
		if minetest.is_creative_enabled(p_name) then
			zr_creative.set_inv(player)
		else
			old_set_inv(player)
		end
	end
else
	minetest.register_on_joinplayer(function(player)
		local p_name = player:get_player_name()
		if minetest.is_creative_enabled(p_name) then
			zr_creative.set_inv(player)
		end
	end)
end

-- Override the hand tool
if minetest.is_creative_enabled("") then
	local digtime = 42
	local caps = {times = {digtime, digtime, digtime}, uses = 0, maxlevel = 256}

	minetest.override_item("", {
		range = 10,
		tool_capabilities = {
			full_punch_interval = 0.5,
			max_drop_level = 3,
			groupcaps = {
				crumbly = caps,
				cracky  = caps,
				snappy  = caps,
				choppy  = caps,
				oddly_breakable_by_hand = caps,
				dig_immediate =
					{times = {[2] = digtime, [3] = 0}, uses = 0, maxlevel = 256},
			},
			damage_groups = {fleshy = 10},
		}
	})
end

-- Unlimited node placement
minetest.register_on_placenode(function(pos, newnode, placer, oldnode, itemstack)
	if placer and placer:is_player() then
		return minetest.is_creative_enabled(placer:get_player_name())
	end
end)

-- Don't pick up if the item is already in the inventory
local old_handle_node_drops = minetest.handle_node_drops
function minetest.handle_node_drops(pos, drops, digger)
	if not digger or not digger:is_player() or
		not minetest.is_creative_enabled(digger:get_player_name()) then
		return old_handle_node_drops(pos, drops, digger)
	end
	local inv = digger:get_inventory()
	if inv then
		for _, item in ipairs(drops) do
			if not inv:contains_item("main", item, true) then
				inv:add_item("main", item)
			end
		end
	end
end

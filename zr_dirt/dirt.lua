
local S = minetest.get_translator("zr_dirt")

zr_dirt.sounds = {
	footstep = {name = "zr_dirt_footstep", gain = 0.4},
	dig = {name = "zr_dirt_dig", gain = 1.0},
	dug = {name = "zr_dirt_footstep", gain = 1.0},
  	place = {name = "zr_dirt_place", gain = 1.0},
}

-- DIRT
minetest.register_node("zr_dirt:dirt", {
    description = S("Dirt"),
    tiles = {"zr_dirt.png"},
    groups = {crumbly = 3, dirt = 1, oddly_breakable_by_hand=2, blast_loss = 4},
    sounds = zr_dirt.sounds,
})
minetest.register_alias("dirt:dirt", "zr_dirt:dirt")
minetest.register_alias("dirt", "zr_dirt:dirt")

minetest.register_node("zr_dirt:dry", {
	description = S("Dry Dirt"),
	tiles = {"zr_dirt_dry.png"},
	groups = {crumbly = 3, dirt = 1, oddly_breakable_by_hand=2, blast_loss = 4},
	sounds = zr_dirt.sounds,
})
minetest.register_alias("dirt:dry", "zr_dirt:dry")

-- DIRT WITH GRASS
zr_dirt.grass_sounds = table.copy(zr_dirt.sounds)
zr_dirt.grass_sounds.footstep = {name = "zr_dirt_grass_footstep", gain = 0.4}

minetest.register_node("zr_dirt:grass", {
    description = S("Dirt with Grass"),
    tiles = {"zr_dirt_grass.png", "zr_dirt.png", 
		{name = "zr_dirt.png^zr_dirt_grass_side.png", tileable_vertical = false}},
    groups = {crumbly = 3, dirt = 1, spread_to_dirt = 1, oddly_breakable_by_hand=2},
    drop = "zr_dirt:dirt",
    sounds = zr_dirt.grass_sounds,
})
minetest.register_alias("dirt:grass", "zr_dirt:grass")

minetest.register_node("zr_dirt:grass_dry", {
	description = S("Dirt with Dry Grass"),
	tiles = {"zr_dirt_grass_dry.png", "zr_dirt_dry.png",
		{name = "zr_dirt_dry.png^zr_dirt_grass_dry_side.png", tileable_vertical = false}},
	groups = {crumbly = 3, dirt = 1, spread_to_dry_dirt = 1, oddly_breakable_by_hand=2},
	drop = "zr_dirt:dry",
	sounds = zr_dirt.grass_sounds,
})
minetest.register_alias("dirt:grass_dry", "zr_dirt:grass_dry")

-- FOREST FLOOR DIRT
minetest.register_node("zr_dirt:litter", {
	description = S("Dirt with leaves and twigs"),
	tiles = {
		"zr_dirt_litter.png", "zr_dirt.png",
		{name = "zr_dirt.png^zr_dirt_litter_side.png", tileable_vertical = false}
	},
	groups = {crumbly = 3, dirt = 1, spread_to_dirt = 1, oddly_breakable_by_hand=2},
	drop = "zr_dirt:dirt",
	sounds = zr_dirt.grass_sounds,
})
minetest.register_alias("dirt:litter", "zr_dirt:litter")

minetest.register_node("zr_dirt:dry_litter", {
	description = S("Dirt with dry leaves and twigs"),
	tiles = {
		"zr_dirt_dry_litter.png", "zr_dirt.png",
		{name = "zr_dirt.png^zr_dirt_dry_litter_side.png", tileable_vertical = false}
	},
	groups = {crumbly = 3, dirt = 1, spread_to_dirt = 1, oddly_breakable_by_hand=2},
	drop = "zr_dirt:dirt",
	sounds = zr_dirt.grass_sounds,
})
minetest.register_alias("dirt:dry_litter", "zr_dirt:dry_litter")

-- DIRT WITH SNOW
minetest.register_node("zr_dirt:snow", {
	description = S("Dirt with Snow"),
	tiles = {"zr_dirt_snow.png", "zr_dirt.png",
		{name = "zr_dirt.png^zr_dirt_snow_side.png",
			tileable_vertical = false}},
	groups = {crumbly = 3, dirt = 1, spread_to_dirt = 1, snowy = 1, oddly_breakable_by_hand=2},
	drop = "zr_dirt:dirt",
	sounds = zr_dirt.grass_sounds,
})
minetest.register_alias("dirt:snow", "zr_dirt:snow")


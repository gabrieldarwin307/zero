Minetest modpack: zero
==========================
See README.txt and license.txt under each individual mod for license information.
License for code is either LGPLv2.1+ or MIT (depending on original license
from "Minetest Game")
License for content is CC 4.0, CC 3.0 by SA, CC 3.0 or CC0 (as specificed
under individual mods)


Authors of source code
----------------------
Written by BigBear (MIT licensed unless otherwise specified)
Many features are derived from "Minetest Game" Originally by celeron55, Perttu Ahola <celeron55@gmail.com> (LGPLv2.1+)
Various Minetest developers and contributors (LGPLv2.1+)
Various Minetest developers and contributors (MIT)

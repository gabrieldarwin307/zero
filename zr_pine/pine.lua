
local S = minetest.get_translator("zr_pine")

minetest.register_node("zr_pine:tree", {
	description = S("Pine Tree"),
	tiles = {"zr_pine_tree_top.png", "zr_pine_tree_top.png",
		"zr_pine_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree = 1, choppy = 3, oddly_breakable_by_hand = 1, flammable = 3},
	sounds = zr_wood.sounds,

	on_place = minetest.rotate_node
})

minetest.register_node("zr_pine:wood", {
	description = S("Pine Wood Planks"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"zr_pine_wood.png"},
	is_ground_content = false,
	groups = {choppy = 3, oddly_breakable_by_hand = 2, flammable = 3, wood = 1},
	sounds = zr_wood.sounds,
})

minetest.register_node("zr_pine:needles",{
	description = S("Pine Needles"),
	drawtype = "allfaces_optional",
	tiles = {"zr_pine_needles.png"},
	waving = 1,
	paramtype = "light",
	is_ground_content = false,
	groups = {snappy = 3, oddly_breakable_by_hand = 2, leafdecay = 3, flammable = 2, leaves = 1},
	drop = {
		max_items = 1,
		items = {
			{items = {"zr_pine:sapling"}, rarity = 20},
			{items = {"zr_pine:needles"}}
		}
	},
	sounds = zr_wood.leaves_sounds,
	after_place_node = zr_wood.after_place_leaves,
})


zr_wood.register_leafdecay({
	trunks = {"zr_pine:tree"},
	leaves = {"zr_pine:needles"},
	radius = 3,
})

minetest.register_craft({
    output = "zr_pine:wood 4",
    recipe = {
        {"zr_pine:tree"},
    }
})


local S = minetest.get_translator("zr_acacia")

minetest.register_node("zr_acacia:tree", {
	description = S("Acacia Tree"),
	tiles = {"zr_acacia_tree_top.png", "zr_acacia_tree_top.png",
		"zr_acacia_tree.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {tree = 1, choppy = 2, oddly_breakable_by_hand = 1, flammable = 2},
	sounds = zr_wood.sounds,

	on_place = minetest.rotate_node
})
minetest.register_alias("acacia:tree", "zr_acacia:tree")

minetest.register_node("zr_acacia:wood", {
	description = S("Acacia Wood Planks"),
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"zr_acacia_wood.png"},
	is_ground_content = false,
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2, wood = 1},
	jounds = zr_wood.sounds,
})
minetest.register_alias("acacia:wood", "zr_acacia:wood")

minetest.register_node("zr_acacia:leaves", {
	description = S("Acacia Tree Leaves"),
	drawtype = "allfaces_optional",
	tiles = {"zr_acacia_leaves.png"},
	special_tiles = {"zr_acacia_leaves_simple.png"},
	waving = 1,
	paramtype = "light",
	is_ground_content = false,
	groups = {snappy = 3, oddly_breakable_by_hand = 2, leafdecay = 3, flammable = 2, leaves = 1},
	drop = {
		max_items = 1,
		items = {
			{items = {"zr_acacia:sapling"}, rarity = 20},
			{items = {"zr_acacia:leaves"}}
		}
	},
	sounds = zr_wood.leaves_sounds,

	after_place_node = zr_wood.after_place_leaves,
})
minetest.register_alias("acacia:leaves", "zr_acacia:leaves")

zr_wood.register_leafdecay({
	trunks = {"zr_acacia:tree"},
	leaves = {"zr_acacia:leaves"},
	radius = 2,
})

minetest.register_craft({
    output = "zr_acacia:wood 4",
    recipe = {
        {"zr_acacia:tree"},
    }
})

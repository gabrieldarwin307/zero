
-- stone stairs (only ran if mod stair is included)
local S = minetest.get_translator("zr_stairs")

-- Register default stairs and slabs

zr_stair.register_all("zr_stone:stone_",{ recipeitem = "zr_stone:stone" }) 
zr_stair.register_all("zr_stone:cobble_",{ recipeitem = "zr_stone:cobble" }) 
zr_stair.register_all("zr_stone:mossycobble_",{ recipeitem = "zr_stone:mossycobble" }) 


local S = minetest.get_translator("zr_gold");

minetest.register_node("bm_limestone:gold_ore", {
	description = S("Copper Ore"),
	tiles = {"zr_lime_stone.png^zr_gold_mineral.png"},
	groups = {cracky = 2},
	drop = "zr_gold:lump",
	sounds = zr_stone.sounds,
})

if (bm_limestone:gold_ore ~= nil) then
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = bm_limestone:gold_ore,
		wherein        = "zr_lime:stone",
		clust_scarcity = 13 * 13 * 13,
		clust_num_ores = 5,
		clust_size     = 3,
		y_max          = 31000,
		y_min          = 1025,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = bm_limestone:gold_ore,
		wherein        = "zr_lime:stone",
		clust_scarcity = 15 * 15 * 15,
		clust_num_ores = 3,
		clust_size     = 2,
		y_max          = -256,
		y_min          = -511,
	})
	minetest.register_ore({
		ore_type       = "scatter",
		ore            = bm_limestone:gold_ore,
		wherein        = "zr_lime:stone",
		clust_scarcity = 13 * 13 * 13,
		clust_num_ores = 5,
		clust_size     = 3,
		y_max          = -512,
		y_min          = -31000,
	})
end

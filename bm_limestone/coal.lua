
local S = minetest.get_translator("zr_coal");

-- coal
minetest.register_node("bm_limestone:coal_ore", {
	description = S("Coal Ore"),
	tiles = {"zr_lime_stone.png^zr_coal_mineral.png"},
	groups = {cracky = 3},
	drop = "zr_coal:lump",
	sounds = zr_stone.sounds,
})

minetest.register_ore({
	ore_type       = "scatter",
	ore            = "bm_limestone:coal_ore",
	wherein        = "zr_lime:stone",
	clust_scarcity = 8 * 8 * 8,
	clust_num_ores = 9,
	clust_size     = 3,
	y_max          = 31000,
	y_min          = 1025,
})
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "bm_limestone:coal_ore",
	wherein        = "zr_lime:stone",
	clust_scarcity = 8 * 8 * 8,
	clust_num_ores = 8,
	clust_size     = 3,
	y_max          = 64,
	y_min          = -127,
})
minetest.register_ore({
	ore_type       = "scatter",
	ore            = "bm_limestone:coal_ore",
	wherein        = "zr_lime:stone",
	clust_scarcity = 12 * 12 * 12,
	clust_num_ores = 30,
	clust_size     = 5,
	y_max          = -128,
	y_min          = -31000,
})

zr_initial_stuff = {}
zr_initial_stuff.setting = minetest.settings:get("zr_initial_stuff")

if zr_initial_stuff.setting then

	zr_initial_stuff.items = zr_initial_stuff.setting:split(",")

	function zr_initial_stuff.give(player) 
		local inv = player:get_inventory()

		for _, item in ipairs(zr_initial_stuff.items) do
			inv:add_item("main", ItemStack(item))
		end
	end

	minetest.register_on_newplayer(zr_initial_stuff.give)
end


local mod_path = minetest.get_modpath("bm_savanna")
dofile(mod_path.."/savanna.lua")

if minetest.get_modpath("zr_farm") then
	zr_farm.add_cotton_to_biome("bm_savanna", "zr_dirt:grass_dry")
end


local mod_path = minetest.get_modpath("bm_limestone")
dofile(mod_path.."/bm_limestone.lua")

if(minetest.get_modpath("zr_coal")) ~= nil then
	dofile(mod_path.."/coal.lua")
end

if(minetest.get_modpath("zr_copper")) ~= nil then
	dofile(mod_path.."/copper.lua")
end

if(minetest.get_modpath("zr_diamond")) ~= nil then
	dofile(mod_path.."/diamond.lua")
end

if(minetest.get_modpath("zr_iron")) ~= nil then
	dofile(mod_path.."/iron.lua")
end

if(minetest.get_modpath("zr_tin")) ~= nil then
	dofile(mod_path.."/tin.lua")
end

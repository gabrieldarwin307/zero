zr_lime = {};

local mod_path = minetest.get_modpath("zr_lime")
dofile(mod_path.."/lime.lua")
dofile(mod_path.."/formation.lua")

if(minetest.get_modpath("zr_stair")) ~= nil then
	dofile(mod_path.."/stair.lua")
end

if(minetest.get_modpath("zr_fence")) ~= nil then
	dofile(mod_path.."/wall.lua")
end

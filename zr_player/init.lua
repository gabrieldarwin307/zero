
zr_player = {}

local mod_path = minetest.get_modpath("zr_player")

dofile(mod_path.."/model.lua")
dofile(mod_path.."/skins.lua")
dofile(mod_path.."/player.lua")
dofile(mod_path.."/hand.lua")
dofile(mod_path.."/commands.lua")

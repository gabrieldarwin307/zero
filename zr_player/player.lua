
local store = minetest.get_mod_storage()

local player_bits = {}
local player_swim = {}
local player_data = {}
local player_wielded = {}

local appearance_change_callback = {}

local appearance_changed = function(player)
	for _, fn in ipairs(appearance_change_callback) do
		fn(player)	
	end
end

local set_animation = function(player, ani_name, lock)
	if not player then return end

	local name = player:get_player_name()
	local p_data = player_data[name]
	if not p_data then return end

	if lock == nil then
		if p_data.locked then return end 
	end
	p_data.locked = lock

	local model = p_data.model or zr_player.models.default
	local controls = player:get_player_control()

	local anim

	if not ani_name then
		if player:get_hp() == 0 then
			anim = model.lie
		elseif controls.up or controls.down or controls.left or controls.right then
			if controls.LMB or controls.RMB then
				if controls.sneak then
					anim = model.sneak_walk_dig
				else
					anim = model.run_dig
				end
			else
				if controls.sneak then
					anim = model.sneak_walk
				else
					if player_swim[name] then
						anim = model.swim or model.run
					else
						anim = model.run
					end
				end
			end
		elseif controls.LMB or controls.RMB then
			if controls.sneak then
				anim = model.sneak_dig
			else
				anim = model.dig
			end
		else
			if controls.sneak then
				anim = model.sneak
			else
				anim = model.stand
			end
		end
	else
		anim = model[ani_name]
	end
	if not anim then return end

	player:set_animation(anim.frame_range, anim.frame_rate)

end

minetest.register_entity("zr_player:wielded_item", {
	initial_properties = {
		pointable = false,
		visual = "wielditem",
		static_save = false
	},

})

local set_wielditem = function(player)
	local p_name = player:get_player_name()
	local p_data = player_data[p_name]
	if not p_data then return end 

	local wield_entity = p_data.wield_entity
	local wield_name = player:get_wielded_item():get_name()
	player_wielded[p_name] = wield_name

	if wield_entity then 
		local props = {}
		if not wield_name or wield_name == "" then
			props.is_visible = false
		else
			props.is_visible = true
			props.textures = { wield_name }
		end

		wield_entity:set_properties(props)
	end
end

local set_model = function(player, modelname) 
	local model = zr_player.models[modelname]
	if not model then return end

	local meta = player:get_meta()
	local p_name = player:get_player_name()
	local p_data = player_data[p_name]

	if not p_data then
		p_data = {}
		player_data[p_name] = p_data
	end

	meta:set_string("zr_player_model", modelname)
	p_data.model = model
	player:set_properties(model.props)
	set_animation(player)

	local wield = p_data.model.wield
	local wield_entity = p_data.wield_entity

	if wield_entity then
		wield_entity:remove()
		p_data.wield_entity = nil
	end

	if wield then 
		if not p_data.wield_entity then
			wield_entity = minetest.add_entity(player:get_pos(), "zr_player:wielded_item")
			p_data.wield_entity = wield_entity
		end

		wield_entity:set_properties({visual_size = wield.size})
		wield_entity:set_attach(player, wield.bone, wield.pos, wield.rot)

		set_wielditem(player)
	end
	appearance_changed(player)
end

local set_skin = function(player, skinname) 
	local p_name = player:get_player_name()
	local p_data = player_data[p_name]
	if not p_data then return end
	local skin = p_data.model.skins[skinname]
	if not skin then return end

	local tex = {}
	for i,v in ipairs(skin) do tex[i] = v end

	if p_data.overlay then
		for _, v in ipairs(overlay) do
			for j, t in ipairs(v) do
				if tex[j] then
					tex[j] = tex[j].."^"..t
				else
					tex[j] = t
				end
			end
		end
	end

	local meta = player:get_meta()
	meta:set_string("zr_player_skin", skinname)

	player:set_properties({ textures= tex })
	appearance_changed(player)
end

local add_overlay = function(player, texture)
	local p_name = player:get_player_name()
	local p_data = player_data[p_name]
	if not p_data then
		p_data = {}
		player_data[p_name] = p_data
	end
	if not p_data.overlay then
		p_data.overlay = {}
	end

	table.insert(p_data.overlay, texture)

	local meta = player:get_meta()
	local skin = meta:get_string("zr_player_skin", skinname) or "default"

	-- re-set current skin to apply new overlay to model
	set_skin(player, skin)
end

local remove_overlay = function(p_name, texture)
	local p_data = player_data[p_name]
	if not p_data or not p_data.overlay then return end

	for i,v in ipairs(p_data.overlay) do
		if #texture ~= #v then break end
		for j, png in v do
			if texture[j] ~= png then break end
		end
		-- no break = same size, and matching elements
		p_data.overlay[i] = nil
		return true
	end
	return false
end


minetest.register_on_joinplayer(function(player)
	minetest.after(1, function () 
		local meta = player:get_meta()
		local model_name = meta:get_string("zr_player_model")
		if not model_name or model_name == "" then
			model_name = "default"
		end
		local skin_name = meta:get_string("zr_player_skin") or "default"
		if not skin_name or skin_name == "" then
			skin_name = "default"
		end

		set_model(player, model_name)
		set_skin(player, skin_name)
	end)
end)

minetest.register_on_leaveplayer(function(player, timed_out)
	local p_name = player:get_player_name()
	local p_data = player_data[p_name]
	if not p_data and p_data.wield_entity then return end
	p_data.wield_entity:remove()
	p_data.wield_entity = nil
end)

minetest.register_on_dieplayer(function(player)
	set_animation(player)
end)

local elapsed = 0.0
minetest.register_globalstep(function(dtime)

	local do_extra_checks = false
	elapsed = elapsed + dtime
	if elapsed > 0.5 then 
		do_extra_checks = true
		elapsed = 0.0
	end
		
	for _, player in pairs(minetest.get_connected_players()) do
		local p_name = player:get_player_name()
		local bits = player:get_player_control_bits()

		if player_bits[p_name] ~= bits then
			set_animation(player)
			player_bits[p_name]=bits
		end

		if do_extra_checks then
			-- its ok if these updates are not immediate
			local wielded = player:get_wielded_item():get_name()
			if wielded ~= player_wielded[p_name] then
				set_wielditem(player)
			end

			local p_pos = player:get_pos()
			local node = minetest.get_node(p_pos)
			local node_def = minetest.registered_nodes[node.name];
			local is_swim = node_def and node_def.liquidtype and node_def.liquidtype ~= "none"
			if is_swim ~= player_swim[p_name] then
				set_animation(player)
				player_swim[p_name] = is_swim
			end
		end
	end
end)

-- Public API
zr_player.set_skin = set_skin
zr_player.set_model = set_model
zr_player.set_animation = set_animation
zr_player.add_overlay = add_overlay
zr_player.remove_overlay = remove_overlay

zr_player.get_skinlist = function(p_name)  
	local p_data = player_data[p_name]
	return p_data and p_data.model and p_data.model.skinlist or ""
end

zr_player.register_on_appearance_set = function(callback)
	table.insert(appearance_change_callback, callback)
end

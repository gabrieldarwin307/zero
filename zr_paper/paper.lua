
local S = minetest.get_translator("zr_paper")

minetest.register_craftitem("zr_paper:paper", {                                                                                                                                                                                           
    description = S("Paper"),
    inventory_image = "zr_paper.png",
    groups = {flammable = 3},
})

minetest.register_craft({
	output = "zr_paper:paper",
	recipe = {
		{"zr_papyrus:papyrus", "zr_papyrus:papyrus", "zr_papyrus:papyrus"},
	}
})

minetest.register_craft({
	type = "fuel",
	recipe = "zr_paper:paper",
	burntime = 1,
})

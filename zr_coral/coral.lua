
local S = minetest.get_translator("zr_coral");

zr_coral.rooted_sounds = zr_dirt.grass_sounds
zr_coral.block_sounds = zr_stone.sounds

local function coral_on_place(itemstack, placer, pointed_thing)
	if pointed_thing.type ~= "node" or not placer then
		return itemstack
	end

	local player_name = placer:get_player_name()
	local pos_under = pointed_thing.under
	local pos_above = pointed_thing.above
	local node_under = minetest.get_node(pos_under)
	local def_under = minetest.registered_nodes[node_under.name]

	if def_under and def_under.on_rightclick and not placer:get_player_control().sneak then
		return def_under.on_rightclick(pos_under, node_under,
				placer, itemstack, pointed_thing) or itemstack
	end

	if node_under.name ~= "zr_coral:skeleton" or
			minetest.get_node(pos_above).name ~= "zr_water:source" then
		return itemstack
	end

		if minetest.is_protected(pos_under, player_name) or
			minetest.is_protected(pos_above, player_name) then
		minetest.log("action", player_name
			.. " tried to place " .. itemstack:get_name()
			.. " at protected position "
			.. minetest.pos_to_string(pos_under))
		minetest.record_protection_violation(pos_under, player_name)
		return itemstack
	end

	node_under.name = itemstack:get_name()
	minetest.set_node(pos_under, node_under)
	if not minetest.is_creative_enabled(player_name) then
		itemstack:take_item()
	end

	return itemstack
end

local default_rooted = {
	drawtype = "plantlike_rooted",
	waving = 1,
	paramtype = "light",
	tiles = {"zr_coral_skeleton.png"},
	groups = {snappy = 3},
	selection_box = {
		type = "fixed",
		fixed = {
				{-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
				{-4/16, 0.5, -4/16, 4/16, 1.5, 4/16},
		},
	},
	node_dig_prediction = "zr_coral:skeleton",
	node_placement_prediction = "",
	sounds = zr_coral.rooted_sounds,
	on_place = coral_on_place,

	after_destruct  = function(pos, oldnode)
		minetest.set_node(pos, {name = "zr_coral:skeleton"})
	end,
}

function zr_coral.register_rooted(coral_name, def) 
	local new_def = table.copy(default_rooted)
	for k, v in pairs(def) do new_def[k] = v end
	minetest.register_node(coral_name, new_def)
end

zr_coral.register_rooted("zr_coral:green", {
	description = S("Green Coral"),
	special_tiles = {{name = "zr_coral_green.png", tileable_vertical = true}},
	inventory_image = "zr_coral_green.png",
	wield_image = "zr_coral_green.png",
})
minetest.register_alias("coral:green", "zr_coral:green")

zr_coral.register_rooted("zr_coral:pink", {
	description = S("Pink Coral"),
	special_tiles = {{name = "zr_coral_pink.png", tileable_vertical = true}},
	inventory_image = "zr_coral_pink.png",
	wield_image = "zr_coral_pink.png",
})
minetest.register_alias("coral:pink", "zr_coral:pink")

zr_coral.register_rooted("zr_coral:cyan", {
	description = S("Cyan Coral"),
	special_tiles = {{name = "zr_coral_cyan.png", tileable_vertical = true}},
	inventory_image = "zr_coral_cyan.png",
	wield_image = "zr_coral_cyan.png",
})
minetest.register_alias("coral:cyan", "zr_coral:cyan")

minetest.register_node("zr_coral:brown", {
	description = S("Brown Coral"),
	tiles = {"zr_coral_brown.png"},
	groups = {cracky = 3},
	drop = "zr_coral:skeleton",
	sounds = zr_coral.block_sounds,
})
minetest.register_alias("coral:brown", "zr_coral:brown")

minetest.register_node("zr_coral:orange", {
	description = S("Orange Coral"),
	tiles = {"zr_coral_orange.png"},
	groups = {cracky = 3},
	drop = "zr_coral:skeleton",
	sounds = zr_coral.block_sounds,
})
minetest.register_alias("coral:orange", "zr_coral:orange")

minetest.register_node("zr_coral:skeleton", {
	description = S("Coral Skeleton"),
	tiles = {"zr_coral_skeleton.png"},
	groups = {cracky = 3},
	sounds = zr_coral.block_sounds,
})
minetest.register_alias("coral:skeleton", "zr_coral:skeleton")




local S = minetest.get_translator("zr_lime")

local function merge_table(def1, def2)
	for k,v in def1 do def_1[k] = v end
	return def_1
end

local def_stalagmite = {
	description = S("Stalagmite"),
	tiles = {"zr_lime_stone.png"},
	special_tiles = {{name = "zr_lime_stalagmite.png"}},
	drawtype = "plantlike_rooted",
	paramtype2 = "leveled",
	paramtype = "light",
	sunlight_propagates = false,
	walkable = true,
	buildable_to = false,
	groups = {cracky = 3, attached_node = 1, oddly_breakable_by_hand=2},
	drop = "zr_lime:cobble",
}

local def_stalactite = {
	description = S("Stalactite"),
	tiles = {"zr_lime_stalagmite.png"},
	drawtype = "plantlike",
	paramtype = "light",
	sunlight_propagates = false,
	walkable = false,
	buildable_to = false,
	groups = {cracky = 3, attached_node = 1, oddly_breakable_by_hand=2},
	drop = "zr_lime:cobble",
}

minetest.register_node("zr_lime:stalagmite", def_stalagmite)
minetest.register_node("zr_lime:stalactite", def_stalactite)

function zr_lime.add_stala_to_biome(biome) 
	minetest.register_decoration({
		name = biome .. ":stalactite",
		deco_type = "simple",
		place_on = {"zr_lime:stone"},
		sidelen = 16,
		noise_params = {
			offset = offset,
			scale = scale,
			spread = {x = 200, y = 200, z = 200},
			seed = 329,
			octaves = 3,
			persist = 0.6
		},
		biomes = { biome }, -- one decoration for each biome
		y_max = -20,
		y_min = -31000,
		param2 = 1,
		param2_max = 32,
		decoration = "zr_lime:stalactite",
		flags = "all_ceilings",
	})

	minetest.register_decoration({
		name = biome .. ":stalagmite",
		deco_type = "simple",
		place_on = {"zr_lime:stone"},
		sidelen = 16,
		noise_params = {
			offset = offset,
			scale = scale,
			spread = {x = 200, y = 200, z = 200},
			seed = 329,
			octaves = 3,
			persist = 0.6
		},
		biomes = { biome }, -- one decoration for each biome
		y_max = -20,
		y_min = -31000,
		param2 = 1,
		param2_max = 32,
		decoration = "zr_lime:stalagmite",
		flags = "all_floors",
	})
end

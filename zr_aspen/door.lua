local S = minetest.get_translator("zr_aspen");

-- GATE
zr_door.register_gate("zr_aspen:gate", {
	description = S("Aspen Wood Fence Gate"),
	texture = "zr_aspen_wood.png",
	material = "zr_aspen:wood",
	groups = {choppy = 2, oddly_breakable_by_hand = 2, flammable = 2},
	sounds = zr_wood.sounds,
})
minetest.register_alias("aspen:gate", "zr_aspen:gate")


minetest.register_privilege("zr_player_basic", "grants players ability to change their own skin and model from a server-defined list")

-- SKINS

local skinme = function(p_name, skin_name)
	local skinlist = zr_player.get_skinlist(p_name)
	
	if not skin_name or skin_name=="" then
		minetest.chat_send_player(p_name, "Available skins:"..skinlist)
	elseif not skinlist:find(skin_name) then
		minetest.chat_send_player(p_name, "Sorry, that skin is not available for your model.")
		minetest.chat_send_player(p_name, "Available skins:"..skinlist)
	else
		local player = minetest.get_player_by_name(p_name)	
		zr_player.set_skin(player, skin_name)
	end
end

minetest.register_chatcommand("skinme", {
	parms = "<skinname>",
	description = "change player skin - or list all skins if no skin is specified",
	privs = {zr_player_basic=true},
	func = skinme,
})

-- create human readable list of skins against each model
for _, model in pairs(zr_player.models) do
	model.skinlist = ""
	for skin_name, _ in pairs(model.skins) do
		model.skinlist = model.skinlist.." "..skin_name
	end
end

-- MODELS

local modelme = function(p_name, model_name)
	if not model_name or model_name=="" then
		minetest.chat_send_player(p_name, "Available models:"..zr_player.modellist)
	elseif not zr_player.models[model_name] then
		minetest.chat_send_player(p_name, "Sorry, that model does not exist on this server.")
	else
		local player = minetest.get_player_by_name(p_name)	
		zr_player.set_model(player, model_name)
		zr_player.set_skin(player, "default")
	end
end

minetest.register_chatcommand("modelme", {
	parms="<modeltype>",
	description="change player model - or list all models if no model is specified",
	privs={ zr_player_basic=true },
	func=modelme,
})

-- create human readible list of model names
local modellist=""
for k,_ in pairs(zr_player.models) do
	modellist=modellist.." "..k
end
zr_player.modellist = modellist

